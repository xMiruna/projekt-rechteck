package logic;

import java.awt.Color;

public class FarbigesRechteck extends Rechteck {

	private Color farbe;

	public Color getFarbe() {
		return farbe;
	}

	public void setFarbe(Color farbe) {
		this.farbe = farbe;
	}

	public FarbigesRechteck(Color farbe) {
		super();
		this.farbe = farbe;
	}
	
	public FarbigesRechteck(Punkt p, int laenge, int hoehe, Color farbe) {
		super(p,laenge, hoehe);
		this.farbe = farbe;
	}
	
}
